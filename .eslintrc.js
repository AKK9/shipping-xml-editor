module.exports = {
    "extends": ["google", "plugin:react/recommended"],
    "env": {
        "browser": true,
        "es6": true,
    },
    "parserOptions": {
        "sourceType": "module",
        "ecmaFeatures": {
            "jsx": true,
        }
    },
    "rules": {
        "max-len": [0],
        "require-jsdoc": [2, {
            require: {
                FunctionDeclaration: false,
                MethodDefinition: false,
                ClassDeclaration: false,
            },
        }]
    },
    plugins: [
        "react"
    ]
};
