const BrandNavTooltipModule = (function() {
    const KEY_TOOLTIP_LAST_SEEN = 'TOOLTIP_LAST_SEEN';

    const _treatAsUTC = function(date) {
        const result = new Date(date);
        result.setMinutes(result.getMinutes() - result.getTimezoneOffset());
        return result;
    };

    const _daysBetween = function(startDate, endDate) {
        const millisecondsPerDay = 24 * 60 * 60 * 1000;
        return (_treatAsUTC(endDate) - _treatAsUTC(startDate)) / millisecondsPerDay;
    };

    const _getLastSeenDate = function() {
        return localStorage.getItem(KEY_TOOLTIP_LAST_SEEN);
    };

    const _updateLastSeenDate = function() {
        localStorage.setItem(KEY_TOOLTIP_LAST_SEEN, new Date());
    };

    const showTheBrandNavTooltipIfItHasBeenMoreThanThirtyDays = function() {
        const lastSeenDate = _getLastSeenDate();
        if (_daysBetween(lastSeenDate, new Date()) > 30) {
            // Show the tooltip
            _updateLastSeenDate();
        }
    };

    return {
        showTheBrandNavTooltipIfItHasBeenMoreThanThirtyDays,
    };
})();

BrandNavTooltipModule.showTheBrandNavTooltipIfItHasBeenMoreThanThirtyDays();
