const XmlModule = (function() {
    const encodeText = function(value) {
        if (typeof value === 'string' || value instanceof String) {
            return value.replace(/&/g, '&amp;')
                .replace(/</g, '&lt;')
                .replace(/>/g, '&gt;')
                .replace(/"/g, '&quot;')
                .replace(/'/g, '&apos;');
        } else {
            return value;
        }
    };

    const cdataText = function(value) {
        if ((typeof value === 'string' || value instanceof String) && value.length > 0) {
            return '<![CDATA[' + value + ']]>';
        } else {
            return value;
        }
    };

    const cleanseValue = function(value) {
        if ((typeof value === 'string' || value instanceof String) && value.length > 0) {
            return value.replace(/<!\[CDATA\[|]]>/g, '');
        } else {
            return value;
        }
    };

    const _propertyToXml = function(key, value) {
        if (value == null || value.length === 0) {
            return '<' + key + ' />\n';
        } else {
            if (key === 'calcodedes-long-returnText' || key === 'calcodedes-displayText') {
                return '<' + key + '>' + cdataText(cleanseValue(value)) + '</' + key + '>\n';
            } else {
                return '<' + key + '>' + encodeText(cleanseValue(value)) + '</' + key + '>\n';
            }
        }
    };

    const itemToXml = function(item) {
        let xml = '    <Shipping>\n';
        xml += '        ' + _propertyToXml('jurstgroup-code', 'UK');
        xml += '        ' + _propertyToXml('jurstgroup-description', 'UK Jurisdiction Group for Shipping');
        xml += '        ' + _propertyToXml('jurst-code', 'UK');
        xml += '        ' + _propertyToXml('jurst-country', 'united kingdom');
        xml += '        ' + _propertyToXml('jurst-countryabbr', 'GB');
        xml += '        ' + _propertyToXml('shipmode-code', item['shipmode-code']);
        xml += '        ' + _propertyToXml('shipmode-carrier', item['shipmode-carrier']);
        xml += '        ' + _propertyToXml('calcode-shipcode', item['calcode-shipcode']);
        xml += '        ' + _propertyToXml('calcode-desc-displayName', item['calcode-desc-displayName']);
        xml += '        ' + _propertyToXml('calcodedes-displayText', item['calcodedes-displayText']);
        xml += '        ' + _propertyToXml('calcodedes-long-returnText', item['calcodedes-long-returnText']);
        xml += '        ' + _propertyToXml('calcode-field1', item['calcode-field1']);
        xml += '        ' + _propertyToXml('calrlookup-shipcharge-value', item['calrlookup-shipcharge-value']);
        xml += '        ' + _propertyToXml('calrule-field1-basketOrder', item['calrule-field1-basketOrder']);
        xml += '        ' + _propertyToXml('calrule-field2-nonpromisedate', item['calrule-field2-nonpromisedate']);
        xml += '    </Shipping>\n';
        return xml;
    };

    const itemsToXml = function(items) {
        let xml = '﻿<?xml version="1.0" encoding="UTF-8"?>';
        xml += '<Shippings loadItemName="Shipping" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="https://s3-eu-west-1.amazonaws.com/dunelm-ap/shipping-xml/shipping.xsd">\n';
        items.forEach(function(item) {
            xml += itemToXml(item);
        });
        xml += '</Shippings>';
        return xml;
    };

    return {
        itemsToXml,
    };
})();

export default XmlModule;
