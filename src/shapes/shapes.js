import PropTypes from 'prop-types';

// noinspection JSUnresolvedFunction
const shipmodeShape = PropTypes.shape({
    'id': PropTypes.number.isRequired,
    'shipmode-code': PropTypes.string.isRequired,
    'shipmode-carrier': PropTypes.string.isRequired,
    'calcode-shipcode': PropTypes.string.isRequired,
    'calcode-desc-displayName': PropTypes.string.isRequired,
    'calcodedes-displayText': PropTypes.string.isRequired,
    'calcodedes-long-returnText': PropTypes.string.isRequired,
    'calcode-field1': PropTypes.string.isRequired,
    'calrlookup-shipcharge-value': PropTypes.number.isRequired,
    'calrule-field1-basketOrder': PropTypes.number.isRequired,
    'calrule-field2-nonpromisedate': PropTypes.string.isRequired,
});

export default shipmodeShape;
