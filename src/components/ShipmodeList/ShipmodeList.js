import React, {Component} from 'react';
import './ShipmodeList.css';
import ShipmodeRow from '../ShipmodeRow/ShipmodeRow';
import shipmodeShape from '../../shapes/shapes';
import PropTypes from 'prop-types';

class ShipmodeList extends Component {
    constructor(props) {
        super(props);
        this.state = {selectedId: props.selectedId};

        this.handleClick = this.handleClick.bind(this);
    }

    componentWillReceiveProps(newProps) {
        this.setState({selectedId: newProps.selectedId});
    }

    handleClick(id) {
        this.props.selectItem(id);
    }

    render() {
        const context = this;
        const items = [];
        this.props.items.forEach(function(item) {
            if (item.id === context.state.selectedId) {
                items.push(
                    <ShipmodeRow
                        item={item}
                        key={item.id}
                        handleClick={context.handleClick}
                        selected={true}
                    />
                );
            } else {
                items.push(
                    <ShipmodeRow
                        item={item}
                        key={item.id}
                        handleClick={context.handleClick}
                    />
                );
            }
        });

        return (
            <div className="shipmode-list">{items}</div>
        );
    }
}

// noinspection JSUnresolvedVariable, JSUnresolvedFunction
ShipmodeList.propTypes = {
    items: PropTypes.arrayOf(shipmodeShape).isRequired,
    selectedId: PropTypes.number.isRequired,
    selectItem: PropTypes.func,
};

export default ShipmodeList;
