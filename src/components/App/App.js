import React, {Component} from 'react';
import './App.css';
import ShipmodeList from '../ShipmodeList/ShipmodeList';
import ShipmodeForm from '../ShipmodeForm/ShipmodeForm';
import XmlModule from '../../modules/XmlModule';
import UtilModule from '../../modules/UtilModule';

class App extends Component {
    constructor(props) {
        super(props);
        const items = [
            {'id': 1, 'shipmode-code': 'Standard_DSV Furniture', 'shipmode-carrier': 'UK Standard', 'calcode-shipcode': 'DSV Furniture', 'calcode-desc-displayName': 'Delivered from supplier', 'calcodedes-displayText': 'Your order has been passed to our supplier. Their courier will be in touch to arrange a convenient delivery date. If you have a query about your order please contact us.', 'calcodedes-long-returnText': '<p>We hope you love this product, but if you decide it\'s not right you can return it for free.  Please contact us to arrange a return via courier.</p><p>Exceptions apply, <a href="/info/help/returns-and-cancellations">see our full returns policy</a>.</p>', 'calcode-field1': '14| |1|18/ZWAN/9570/30225994', 'calrlookup-shipcharge-value': 9.95, 'calrule-field1-basketOrder': 4, 'calrule-field2-nonpromisedate': ''},
            {'id': 2, 'shipmode-code': 'Standard_2 Man DSV Short', 'shipmode-carrier': 'UK Standard', 'calcode-shipcode': '2 Man DSV Short', 'calcode-desc-displayName': 'Standard Delivery', 'calcodedes-displayText': 'Your order has been passed to our supplier. Their courier will be in touch to arrange a convenient delivery date. If you have a query about your order please contact us.', 'calcodedes-long-returnText': '<p>We hope you love this product, but if you decide it\'s not right you can return it for free.  Please contact us to arrange a return via courier.</p><p>Exceptions apply, <a href="/info/help/returns-and-cancellations">see our full returns policy</a>.</p>', 'calcode-field1': '2| |1|19/ZWAN/9570/30225994', 'calrlookup-shipcharge-value': 9.95, 'calrule-field1-basketOrder': 5, 'calrule-field2-nonpromisedate': ''},
            {'id': 3, 'shipmode-code': 'Standard_2 Man DSV Normal', 'shipmode-carrier': 'UK Standard', 'calcode-shipcode': '2 Man DSV Normal', 'calcode-desc-displayName': 'Standard Delivery', 'calcodedes-displayText': 'Your order has been passed to our supplier. Their courier will be in touch to arrange a convenient delivery date. If you have a query about your order please contact us.', 'calcodedes-long-returnText': '<p>We hope you love this product, but if you decide it\'s not right you can return it for free.  Please contact us to arrange a return via courier.</p><p>Exceptions apply, <a href="/info/help/returns-and-cancellations">see our full returns policy</a>.</p>', 'calcode-field1': '5| |2|19/ZWAN/9570/30225994', 'calrlookup-shipcharge-value': 9.95, 'calrule-field1-basketOrder': 6, 'calrule-field2-nonpromisedate': ''},
            {'id': 4, 'shipmode-code': 'Standard_2 Man DSV Medium', 'shipmode-carrier': 'UK Standard', 'calcode-shipcode': '2 Man DSV Medium', 'calcode-desc-displayName': 'Standard Delivery', 'calcodedes-displayText': 'Your order has been passed to our supplier. Their courier will be in touch to arrange a convenient delivery date. If you have a query about your order please contact us.', 'calcodedes-long-returnText': '<p>We hope you love this product, but if you decide it\'s not right you can return it for free.  Please contact us to arrange a return via courier.</p><p>Exceptions apply, <a href="/info/help/returns-and-cancellations">see our full returns policy</a>.</p>', 'calcode-field1': '30| |10|19/ZWAN/9570/30225994', 'calrlookup-shipcharge-value': 9.95, 'calrule-field1-basketOrder': 7, 'calrule-field2-nonpromisedate': ''},
            {'id': 5, 'shipmode-code': 'Standard_2 Man DSV Long', 'shipmode-carrier': 'UK Standard', 'calcode-shipcode': '2 Man DSV Long', 'calcode-desc-displayName': 'Standard Delivery', 'calcodedes-displayText': 'Your order has been passed to our supplier. Their courier will be in touch to arrange a convenient delivery date. If you have a query about your order please contact us.', 'calcodedes-long-returnText': '<p>We hope you love this product, but if you decide it\'s not right you can return it for free.  Please contact us to arrange a return via courier.</p><p>Exceptions apply, <a href="/info/help/returns-and-cancellations">see our full returns policy</a>.</p>', 'calcode-field1': '70| |20|19/ZWAN/9570/30225994', 'calrlookup-shipcharge-value': 9.95, 'calrule-field1-basketOrder': 8, 'calrule-field2-nonpromisedate': ''},
            {'id': 6, 'shipmode-code': 'Standard_Dunelm Furniture', 'shipmode-carrier': 'UK Standard', 'calcode-shipcode': 'Dunelm Furniture', 'calcode-desc-displayName': 'Furniture delivery', 'calcodedes-displayText': 'Our courier will call you in 2 - 3 days to arrange a convenient delivery date.', 'calcodedes-long-returnText': '<p>We hope you love this product, but if you decide it\'s not right you can return it for free.  Please contact us to arrange a return via courier.</p><p>Exceptions apply, <a href="/info/help/returns-and-cancellations">see our full returns policy</a>.</p>', 'calcode-field1': '10| |3|18/ZWAN/9570/30034608', 'calrlookup-shipcharge-value': 9.95, 'calrule-field1-basketOrder': 9, 'calrule-field2-nonpromisedate': ''},
            {'id': 7, 'shipmode-code': 'Standard_Saturday Nom Day', 'shipmode-carrier': 'UK Standard', 'calcode-shipcode': 'Saturday Nom Day', 'calcode-desc-displayName': 'Furniture delivery', 'calcodedes-displayText': 'We’ll text you the day before your delivery with a 4 hour time slot.', 'calcodedes-long-returnText': '<p>We hope you love this product, but if you decide it\'s not right you can return it for free.  Please contact us to arrange a return via courier.</p><p>Exceptions apply, <a href="/info/help/returns-and-cancellations">see our full returns policy</a>.</p>', 'calcode-field1': '10| |3|18/ZWAN/9570/30034608', 'calrlookup-shipcharge-value': 14.95, 'calrule-field1-basketOrder': 10, 'calrule-field2-nonpromisedate': ''},
            {'id': 8, 'shipmode-code': 'Standard_Furniture Nom Day', 'shipmode-carrier': 'UK Standard', 'calcode-shipcode': 'Furniture Nom Day', 'calcode-desc-displayName': 'Furniture delivery', 'calcodedes-displayText': 'We’ll text you the day before your delivery with a 4 hour time slot.', 'calcodedes-long-returnText': '<p>We hope you love this product, but if you decide it\'s not right you can return it for free.  Please contact us to arrange a return via courier.</p><p>Exceptions apply, <a href="/info/help/returns-and-cancellations">see our full returns policy</a>.</p>', 'calcode-field1': '7| |3|18/ZWAN/9570/30034608|SAT|SUN', 'calrlookup-shipcharge-value': 9.95, 'calrule-field1-basketOrder': 11, 'calrule-field2-nonpromisedate': '14/04/2017|17/04/2017|01/05/2017|29/05/2017|28/08/2017|25/12/2017|26/12/2017'},
            {'id': 9, 'shipmode-code': 'Standard_Gift Voucher', 'shipmode-carrier': 'UK Standard', 'calcode-shipcode': 'Gift Voucher', 'calcode-desc-displayName': 'Royal Mail', 'calcodedes-displayText': 'We\'ll let you know when your order is on its way to you.', 'calcodedes-long-returnText': '<p>We are unable to refund the value of Dunelm gift cards.</p>', 'calcode-field1': '5| |1|13/ZWAS/7960/30281259', 'calrlookup-shipcharge-value': 0, 'calrule-field1-basketOrder': 12, 'calrule-field2-nonpromisedate': ''},
            {'id': 10, 'shipmode-code': 'Standard_Fabric Samples', 'shipmode-carrier': 'UK Standard', 'calcode-shipcode': 'Fabric Samples', 'calcode-desc-displayName': 'Royal Mail', 'calcodedes-displayText': 'We\'ll let you know when your order is on its way to you.', 'calcodedes-long-returnText': '<p>We are unable to accept returned fabric samples.</p>', 'calcode-field1': '5| |1|14/ZWFR/7960/30281260', 'calrlookup-shipcharge-value': 0, 'calrule-field1-basketOrder': 13, 'calrule-field2-nonpromisedate': ''},
            {'id': 11, 'shipmode-code': 'Standard_Made to Order', 'shipmode-carrier': 'UK Standard', 'calcode-shipcode': 'Made to Order', 'calcode-desc-displayName': 'Made to order delivery', 'calcodedes-displayText': 'Before we process your order there is a 48 hour cooling off period. If you change your mind within this time, call our Customer Service Team on 0344 346 0022. After this point we will begin creating your made to order item(s).', 'calcodedes-long-returnText': '<p>We strongly recommend ordering a free fabric sample before you place your upholstery order to ensure colour and texture accuracy. Viewing fabrics on a screen does not offer a true representation, as monitors vary in brightness and colour saturation. Before placing your order, check the measurements of the product to ensure the size is suitable for your home and needs.</p><p>After placing your custom made order with us, there is a 48 hour \'cooling off\' period. During this time your order will not be processed and you may cancel or amend it by calling our Customer Services team on 0344 346 0022. After the 48 hour \'cooling off\' period, we will begin to create your made to order furniture and a request to amend the upholstery item(s) will result in the order being cancelled. Once the 48 hour cooling off period has passed, our 28 day goodwill returns policy will not apply. This does not affect your <a href="/info/help/returns-and-cancellations">statutory cancellation rights</a>, or your rights in respect of products that are faulty, damaged or not made to specification.</p>', 'calcode-field1': '40| |5|19/ZWTS/7960/30281256', 'calrlookup-shipcharge-value': 9.95, 'calrule-field1-basketOrder': 14, 'calrule-field2-nonpromisedate': ''},
            {'id': 12, 'shipmode-code': 'Express', 'shipmode-carrier': 'Express', 'calcode-shipcode': 'Express', 'calcode-desc-displayName': 'Express delivery', 'calcodedes-displayText': 'We\'ll let you know when your order is on its way to you. Our couriers will contact you with the progress of your delivery.', 'calcodedes-long-returnText': '', 'calcode-field1': '1|21:00|0|12/ZWAN/9578/30034606|SAT|SUN', 'calrlookup-shipcharge-value': 5.95, 'calrule-field1-basketOrder': 0, 'calrule-field2-nonpromisedate': '14/04/2017|17/04/2017|01/05/2017|29/05/2017|28/07/2017|25/12/2017|26/12/2017'},
            {'id': 13, 'shipmode-code': 'PickupInStore', 'shipmode-carrier': '', 'calcode-shipcode': 'ROCS', 'calcode-desc-displayName': 'Reserve & Collect', 'calcodedes-displayText': 'collect after 3 hours', 'calcodedes-long-returnText': '<p>We hope you love this product, but if you decide it\'s not right you can return it for free to your local store.</p><p>Exceptions apply,<a href="/info/help/returns-and-cancellations"> see our full returns policy</a>.</p>', 'calcode-field1': '1| |0| |', 'calrlookup-shipcharge-value': 0, 'calrule-field1-basketOrder': 0, 'calrule-field2-nonpromisedate': ''},
            {'id': 14, 'shipmode-code': 'Standard_1 Man DSV', 'shipmode-carrier': 'UK Standard', 'calcode-shipcode': '1 Man DSV', 'calcode-desc-displayName': 'Delivered from supplier', 'calcodedes-displayText': 'Your order has been passed to our supplier. Their courier will contact you with the progress of your delivery. If you have a query about your order please contact us.', 'calcodedes-long-returnText': '<p>We hope you love this product, but if you decide it\'s not right you can return it for free, here\'s how:</p>            <strong>Store return </strong>- If you live near one of our stores and can easily carry your item, this is the fastest way to return or exchange a product. If you are unable to return your order to a Store, Please contact us to arrange a return via a Courier </p></p><p>Exceptions apply, <a href="/info/help/returns-and-cancellations">see our full returns policy</a>.</p>', 'calcode-field1': '8| |2|21/ZWAN/9570/30281257', 'calrlookup-shipcharge-value': 3.95, 'calrule-field1-basketOrder': 3, 'calrule-field2-nonpromisedate': ''},
            {'id': 15, 'shipmode-code': 'Standard_1 Man DSV Short', 'shipmode-carrier': 'UK Standard', 'calcode-shipcode': '1 Man DSV Short', 'calcode-desc-displayName': 'Delivered from supplier', 'calcodedes-displayText': 'Your order has been passed to our supplier. Their courier will contact you with the progress of your delivery. If you have a query about your order please contact us.', 'calcodedes-long-returnText': '<p>We hope you love this product, but if you decide it\'s not right you can return it for free, here\'s how:</p>            <strong>Store return </strong>- If you live near one of our stores and can easily carry your item, this is the fastest way to return or exchange a product. If you are unable to return your order to a Store, Please contact us to arrange a return via a Courier </p></p><p>Exceptions apply, <a href="/info/help/returns-and-cancellations">see our full returns policy</a>.</p>', 'calcode-field1': '5| |2|21/ZWAN/9570/30281257', 'calrlookup-shipcharge-value': 3.95, 'calrule-field1-basketOrder': 2, 'calrule-field2-nonpromisedate': ''},
            {'id': 16, 'shipmode-code': 'Standard_SAMREXDHL', 'shipmode-carrier': 'DHL', 'calcode-shipcode': 'SAMREXDHL', 'calcode-desc-displayName': 'Made to Measure Delivery', 'calcodedes-displayText': '', 'calcodedes-long-returnText': '', 'calcode-field1': '12| |2|20/ZWFR/7960/30281255', 'calrlookup-shipcharge-value': 5.95, 'calrule-field1-basketOrder': 15, 'calrule-field2-nonpromisedate': ''},
            {'id': 17, 'shipmode-code': 'Standard_Standard NONPOD', 'shipmode-carrier': 'UK Standard', 'calcode-shipcode': 'Standard NONPOD', 'calcode-desc-displayName': 'Standard delivery', 'calcodedes-displayText': 'We\'ll let you know when your order is on its way to you. Our couriers will contact you with the progress of your delivery.', 'calcodedes-long-returnText': '<p>We hope you love this product, but if you decide it\'s not right you can return it for free, here\'s how:</p><strong>Store return </strong>- If you live near one of our stores and can easily carry your item, this is the fastest way to return or exchange a product. </p><strong>Collect+ return </strong>- For products that weigh less than 10kg and are smaller than 60x50x50cm, you can return your item at thousands of collection points around the country. Complete the returns form provided with your delivery and enclose with your parcel. <a href="http://www.collectplus.co.uk/returns/new/dunelm" rel="nofollow" target="_blank">Locate your local collect+ point.</a></p><p>If your parcel weighs more than 10kg and is larger than 60cmx50cmx50cm, please contact us to arrange a return.</p><p>Exceptions apply, <a href="/info/help/returns-and-cancellations">see our full returns policy</a>.</p>', 'calcode-field1': '5| |2|11/ZWAN/9578/30034607', 'calrlookup-shipcharge-value': 3.95, 'calrule-field1-basketOrder': 1, 'calrule-field2-nonpromisedate': ''},
        ];
        this.state = {
            items: items,
            selectedItem: items[0],
        };

        this.getItemIndexById = this.getItemIndexById.bind(this);
        this.updateItem = this.updateItem.bind(this);
        this.selectItem = this.selectItem.bind(this);
        this.generateXml = this.generateXml.bind(this);
        this.importXml = this.importXml.bind(this);
    }

    getItemIndexById(id) {
        const items = this.state.items;
        let itemIndex = null;
        items.forEach(function(item, index) {
            if (item.id === id) {
                itemIndex = index;
            }
        });
        return itemIndex;
    }

    updateItem(item) {
        const index = this.getItemIndexById(item.id);
        const items = this.state.items;
        items[index] = item;
        this.setState(items);
    }

    selectItem(id) {
        const item = this.state.items[this.getItemIndexById(id)];
        this.setState({selectedItem: item});
    }

    generateXml() {
        const xml = XmlModule.itemsToXml(this.state.items);
        UtilModule.downloadFile('shipping.xml', xml);
    }

    importXml() {
        // do something
    }

    render() {
        return (
            <div className="app">
                <div className="toolbar">
                    <div className="logo-container">
                        <div className="logo-container__title">Shipping XML Editor</div>
                    </div>
                    <div className="menu-container">
                        <div className="menu-container__menu-item" onClick={this.importXml}>Import</div>
                        <div className="menu-container__menu-item" onClick={this.generateXml}>Export</div>
                    </div>
                </div>
                <div className="workspace">
                    <ShipmodeList selectedId={this.state.selectedItem.id} items={this.state.items} selectItem={this.selectItem} />
                    <ShipmodeForm item={this.state.selectedItem} updateItem={this.updateItem} />
                </div>
            </div>
        );
    }
}

export default App;
