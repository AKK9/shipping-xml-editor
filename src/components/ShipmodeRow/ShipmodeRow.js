import React, {Component} from 'react';
import './ShipmodeRow.css';
import PropTypes from 'prop-types';
import shipmodeShape from '../../shapes/shapes';

class ShipmodeRow extends Component {
    constructor(props) {
        super(props);
        this.state = {
            item: props.item,
            selected: props.selected,
        };

        this.handleClick = this.handleClick.bind(this);
    }

    componentWillReceiveProps(newProps) {
        this.setState({selected: newProps.selected});
    }

    handleClick() {
        this.props.handleClick(this.state.item.id);
    }

    render() {
        let classes = 'shipmode-row';
        if (this.state.selected === true) {
            classes += ' isSelected';
        }
        return (
            <div className={classes} onClick={this.handleClick}>
                <div className="shipmode-title-container">
                    <div id="calcode-shipcode" className="shipmode-title-container__title">{this.state.item['calcode-shipcode']}</div>
                    <div id="calcode-desc-displayName" className="shipmode-title-container__subtitle">{this.state.item['calcode-desc-displayName']}</div>
                </div>
                <div className="shipmode-detail-container">
                    <div id="calrule-field1-basketOrder" className="shipmode-detail-container__item">{this.state.item['calrule-field1-basketOrder']}</div>
                    <div id="calcode-field1" className="shipmode-detail-container__item">{this.state.item['calcode-field1']}</div>
                </div>
            </div>
        );
    }
}

// noinspection JSUnresolvedVariable
ShipmodeRow.propTypes = {
    item: shipmodeShape.isRequired,
    selected: PropTypes.bool,
    handleClick: PropTypes.func.isRequired,
};

export default ShipmodeRow;
